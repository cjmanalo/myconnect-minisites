#pragma checksum "C:\Workspace\myconnect-minisites\astute\Views\Home\Faq.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "8fd431e53617daa82fabf0ebb55f2d34fedc1889"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Faq), @"mvc.1.0.view", @"/Views/Home/Faq.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Home/Faq.cshtml", typeof(AspNetCore.Views_Home_Faq))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Workspace\myconnect-minisites\astute\Views\_ViewImports.cshtml"
using astute;

#line default
#line hidden
#line 2 "C:\Workspace\myconnect-minisites\astute\Views\_ViewImports.cshtml"
using astute.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"8fd431e53617daa82fabf0ebb55f2d34fedc1889", @"/Views/Home/Faq.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"6d10b8a082d0ad19b4e793ac146ec9eca009923b", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Faq : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(0, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 2 "C:\Workspace\myconnect-minisites\astute\Views\Home\Faq.cshtml"
  
    ViewData["Title"] = "Frequently Asked Questions";

#line default
#line hidden
            BeginContext(64, 4964, true);
            WriteLiteral(@"
<section class=""container page-header"">



    <h1>
        <span>
            Frequently Asked Questions
        </span>
    </h1>




    <img width=""1440"" height=""615"" src=""/wp-content/uploads/2016/06/sub_faqs-1440x615.jpg"" class=""attachment-banner-image size-banner-image wp-post-image"" alt=""sub_faqs"" />



</section>








<section class=""container padding content"">

    <div class=""row"">




    </div>



</section>













<section class=""container padding-bottom faqs"">

    <div class=""row"">

        <div id=""accordion"">




            <div class=""draw-outer"">

                <h4>Who does Astute Connect work with?</h4>

                <div class=""draw"" style=""display: none;"">

                    <p>AGL, Origin, Energy Australia, ActewAGL, Powershop, Sumo Power, Red Energy, Telstra, TPG, NetCube, Foxtel and CGU.</p>

                </div>

            </div>


            <div class=""draw-outer"">

                <h");
            WriteLiteral(@"4>What&#8217;s the latest time I can organise my electricity and/or gas?</h4>

                <div class=""draw"" style=""display: none;"">

                    <p>The latest time an electricity or gas company can receive a request to connect power is midday for a next day connection. In certain areas we can arrange same day connections.</p>

                </div>

            </div>


            <div class=""draw-outer"">

                <h4>When can I expect to be contacted?</h4>

                <div class=""draw"" style=""display: none;"">

                    <p>We guarantee we can contact you by the end of the next business day.</p>

                </div>

            </div>


            <div class=""draw-outer"">

                <h4>What happens after I submit an online request for connection?</h4>

                <div class=""draw"" style=""display: none;"">

                    <p>Once we receive your submission, we will give you a call to confirm your details and the utilities ");
            WriteLiteral(@"you wish to connect. We then contact our partners to process your requests.</p>

                </div>

            </div>


            <div class=""draw-outer"">

                <h4>Why do I need to provide personal information?</h4>

                <div class=""draw"" style=""display: none;"">

                    <p>MyConnect will ask for your ID number, date of birth etc. We do not disclose any personal information about our customers for purposes other than arranging their connection, and we need to obtain your consent to do so. This information is required to fulfil the service request. All personal information disclosure complies with the Australian Privacy Principles under the Privacy Act 1988.</p>

                </div>

            </div>


            <div class=""draw-outer"">

                <h4>How do I find my electricity or gas meter?</h4>

                <div class=""draw"" style=""display: none;"">

                    <p>There needs to be clear access to your meters on ");
            WriteLiteral(@"the day of your connection. Your meter will differ depending on the style of your property. If you live in an apartment, unit or townhouse, it is likely that all of the meters are located in one central area. In older unit blocks there may just be one meter for all the properties. In a house, the electricity and gas meter is usually near the front door or round the side of the house.</p>

                </div>

            </div>


            <div class=""draw-outer"">

                <h4>I need to change my connection details.</h4>

                <div class=""draw"" style=""display: none;"">

                    <p>This is easily done. Please call us on <strong>1300 854 478</strong>.</p>

                </div>

            </div>


            <div class=""draw-outer"">

                <h4>I have a billing enquiry</h4>

                <div class=""draw"" style=""display: none;"">

                    <p>If you have a billing enquiry, please contact us direct on 1300 854 478.</p>

    ");
            WriteLiteral(@"            </div>

            </div>


            <div class=""draw-outer"">

                <h4>How is this service free?</h4>

                <div class=""draw"" style=""display: none;"">

                    <p>We receive a fee for every connection we make with one of our partners. This does not affect the price you pay to them.</p>

                </div>

            </div>


            <div class=""draw-outer"">

                <h4>How do I get connected?</h4>

                <div class=""draw"" style=""display: none;"">

                    <p>To begin the process, you can either fill out an online connection form and let us call you, or you can call us on 1300 854 478 between 9am-6pm weekdays and 10am-3pm on Saturdays.</p>

                </div>

            </div>




        </div>


    </div>

</section>


");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
