﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using astute.Models;
using Microsoft.AspNetCore.Mvc;

namespace astute.Controllers
{
    public class HomeController : Controller
    {
        private AgencyDetailsModel model;

        public HomeController()
        {
            using (var stream = new StreamReader("astute.json"))
            {
                var configurationContent = stream.ReadToEnd();
                this.model = Newtonsoft.Json.JsonConvert.DeserializeObject<AgencyDetailsModel>(configurationContent);
            }
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            var x = HttpContext.Request.Host;
            return View(this.model);
        }

        public IActionResult Contact()
        {
            return View();
        }

        public IActionResult Terms()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult Faq()
        {
            return View();
        }

        public IActionResult HowItWorks()
        {
            return View();
        }

        public IActionResult MovingChecklist()
        {
            return View();
        }
    }
}