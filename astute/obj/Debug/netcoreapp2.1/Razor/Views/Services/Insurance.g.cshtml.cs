#pragma checksum "C:\Workspace\myconnect-minisites\astute\Views\Services\Insurance.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "93a4d60072f46d35e72d1840dee984c179796f2e"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Services_Insurance), @"mvc.1.0.view", @"/Views/Services/Insurance.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Services/Insurance.cshtml", typeof(AspNetCore.Views_Services_Insurance))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Workspace\myconnect-minisites\astute\Views\_ViewImports.cshtml"
using astute;

#line default
#line hidden
#line 2 "C:\Workspace\myconnect-minisites\astute\Views\_ViewImports.cshtml"
using astute.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"93a4d60072f46d35e72d1840dee984c179796f2e", @"/Views/Services/Insurance.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"6d10b8a082d0ad19b4e793ac146ec9eca009923b", @"/Views/_ViewImports.cshtml")]
    public class Views_Services_Insurance : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("name", "AdditionalServices", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.PartialTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_PartialTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(0, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 2 "C:\Workspace\myconnect-minisites\astute\Views\Services\Insurance.cshtml"
  
    ViewData["Title"] = "Insurance";

#line default
#line hidden
            BeginContext(47, 4069, true);
            WriteLiteral(@"
<section class=""container page-header"">



    <h1>
        <span>
            Insurance
        </span>
    </h1>




    <img width=""1440"" height=""615"" src=""/wp-content/uploads/2016/06/sub_insurance-1440x615.jpg"" class=""attachment-banner-image size-banner-image wp-post-image"" alt=""sub_insurance"" />



</section>


<section class=""container padding content center-align"">

    <div class=""row"">


        <p>We understand that it is important to feel safe and protected. To help ensure peace of mind, we have partnered with CGU insurance to provide our customers with the highest protection quality and value.</p>





    </div>

</section>


<section class=""service-menu"">

    <div class=""row inner"">


        <ul id=""tabs"">


            <li><a href=""#service-1"">CGU</a></li>


        </ul>


    </div>

</section>



<section class=""container padding service-brands"">


    <div id=""content"" class=""tabs"">



        <div id=""service-1"" class=""tab ");
            WriteLiteral(@"row inner"" style=""display: none;"">

            <div class=""span_5 center-align"">


                <img src=""/wp-content/uploads/2016/06/logos_CGU_2016-300x160.gif"" alt="""" />


            </div>

            <div class=""span_7"">

                <h4>CGU</h4>

                <p>Each year CGU provide insurance protection for nearly half a million properties in Australia. They pride themselves on their 160-year history of helping customers and thus feel they have earned a high level of trust. CGU pay on average 98 per cent of all claims received</p>









                <a class=""button"" data-remodal-target=""other-site"">Get your FREE quote!</a>




                <!-- modal window for ""other-site"" -->
                <div class=""remodal other-site"" data-remodal-id=""other-site"" data-remodal-options=""hashTracking: false, closeOnOutsideClick: true"">

                    <button data-remodal-action=""close"" class=""remodal-close""></button>

                    <h1>Please note!");
            WriteLiteral(@"</h1>


                    <div class=""content"">

                        <p>You are about to leave this website and enter a site operated by CGU Insurance Ltd.  Any reference to ‘us’, ‘we’ and ‘our’ is a reference to CGU.</p>

                        <a class=""button"" href=""http://www.easysure.com.au/MYCB2C/MYCB2C/hh.asp"">Continue</a>

                    </div>

                </div>











            </div>

        </div>





    </div>



</section>







<section class=""container content important-information padding"">
    <div class=""row"">
        <div id=""accordion"">

            <h4>Disclosure Statement</h4>
            <div class=""draw"" style=""display:none;"">
                <p class=""p1""><span class=""s1""><i>MYCONNECT PTY LTD ABN 34 121 892 331 (“MyConnect”) has an agreement with Insurance Australia Limited ABN 11 000 016 722 AFS Licence No. 227681 trading as CGU Insurance (“CGU”) for the distribution of general insurance products. MyConnect ");
            WriteLiteral(@"is entitled to receive from CGU a commission for insurance products issued pursuant to this agreement.  Please contact MyConnect on 1300 854 478 for further information.  Insurance products are issued by CGU. A product disclosure statement for the insurance products is available here: </i><a href=""http://www.cgu.com.au/insurance/sites/default/files/Basic%20Cover%20Fundamentals%20PDS.pdf""><span class=""s2""><i>fundamentals</i></span></a><i> PDS, </i><a href=""http://www.cgu.com.au/insurance/sites/default/files/Quality%20Cover%20Listed%20Events%20PDS.PDF""><span class=""s2""><i>Listed Events</i></span></a><i> PDS, </i><a href=""http://www.cgu.com.au/insurance/sites/default/files/Premium%20Cover%20Accidental%20Damage%20PDS.PDF""><span class=""s2""><i>Accidental Damage</i></span></a><i>PDS. You should consider the product disclosure statement before making a decision in regard to your insurance needs.</i></span></p>
            </div>

        </div>
    </div>

</section>








");
            EndContext();
            BeginContext(4116, 37, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("partial", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "ba08d6df8623497d9b8ab027072f5638", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_PartialTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.PartialTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_PartialTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_PartialTagHelper.Name = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
