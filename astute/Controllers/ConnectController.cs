﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace astute.Controllers
{
    public class ConnectController : Controller
    {

        public IActionResult Index()
        {
            return View();
        }

        public class ResponseObject
        {
            public bool Success { get; set; }
            public string Message { get; set; }
            public string Reference { get; set; }
        }

        [HttpPost]
        public async Task<IActionResult> Form()
        {
            var fields = Request.Form;

            HttpClient client = new HttpClient();

            string token = "xxx";
               // build the correct token
            client.DefaultRequestHeaders.Add("Authorization", $"Bearer {token}");

            // build the fields to send to the api
            List<KeyValuePair<string, string>> values = new List<KeyValuePair<string, string>>();
            foreach (var item in fields)
            {
                var fieldName = item.Key;
                var value = item.Value;
                values.Add(new KeyValuePair<string, string>(fieldName, value));
            }

            var response = await client.PostAsJsonAsync("http://test.services.myconnect.com.au", values);
            if (response.IsSuccessStatusCode)
            {
                // it worked
                var jsonString = await response.Content.ReadAsStringAsync();
                var result = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseObject>(jsonString);

                if (result.Success)
                    return View(result);
                else
                {
                    throw new Exception(result.Message);
                }
            }
            else
            {
                throw new Exception("it didnt");
            }
        }
    }
}