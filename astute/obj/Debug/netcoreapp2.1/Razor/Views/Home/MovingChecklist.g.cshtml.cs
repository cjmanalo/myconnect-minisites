#pragma checksum "C:\Workspace\myconnect-minisites\astute\Views\Home\MovingChecklist.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "f5ed0c6f6c0dd1e2adb2779b0b37789c51d8b804"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_MovingChecklist), @"mvc.1.0.view", @"/Views/Home/MovingChecklist.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Home/MovingChecklist.cshtml", typeof(AspNetCore.Views_Home_MovingChecklist))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Workspace\myconnect-minisites\astute\Views\_ViewImports.cshtml"
using astute;

#line default
#line hidden
#line 2 "C:\Workspace\myconnect-minisites\astute\Views\_ViewImports.cshtml"
using astute.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"f5ed0c6f6c0dd1e2adb2779b0b37789c51d8b804", @"/Views/Home/MovingChecklist.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"6d10b8a082d0ad19b4e793ac146ec9eca009923b", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_MovingChecklist : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(0, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 2 "C:\Workspace\myconnect-minisites\astute\Views\Home\MovingChecklist.cshtml"
  
    ViewData["Title"] = "Moving Checklist";

#line default
#line hidden
            BeginContext(54, 5094, true);
            WriteLiteral(@"
<section class=""container page-header"">



    <h1>
        <span>
            Moving checklist
        </span>
    </h1>




    <img width=""1440"" height=""615"" src=""http://astute.myconnect.com.au/wp-content/uploads/2016/06/sub_moving_checklist-1440x615.jpg"" class=""attachment-banner-image size-banner-image wp-post-image"" alt=""sub_moving_checklist"" />



</section>








<section class=""container padding content"">

    <div class=""row"">


        <ul class=""styled"">
            <li>
                <h3><strong>4</strong> Weeks before the move</h3>
                <ul>
                    <li>Arrange to settle all outstanding bills</li>
                    <li>Start collecting boxes</li>
                    <li>Notify your real estate agent/landlord of your moving date</li>
                    <li>If you require a removalist, organise them for the required date. Otherwise, consider booking a truck or trailer for your move</li>
                    <li>If you require a c");
            WriteLiteral(@"leaner or gardener, organise them for the required date</li>
                    <li>Begin packing all the items you don&#8217;t need until you move to your new property</li>
                    <li>Take note of what items are fragile or need bubble wrapping/special care</li>
                    <li>Dispose of your unwanted items &#8211; donate, throw out or sell.</li>
                    <li>If required, book storage facilities</li>
                </ul>
            </li>
            <li>
                <h3><strong>3</strong> Weeks before the move</h3>
                <ul>
                    <li>Attach inventory lists to each box as you pack them, including what room they&#8217;re for.</li>
                    <li>Organise important paperwork into a box you can easily access when needed.</li>
                    <li>If you have pets, notify your new council for re-registering.</li>
                    <li>Notify anyone who needs to know of your new address, such as banks, employers, the ATO et");
            WriteLiteral(@"c. Australia Post&#8217;s Notify Organisations service can help you arrange address notifications and mail redirections for bank, memberships, car registration/driver&#8217;s license, Medicare, superannuation, insurance, frequent flyer and other loyalty programs.</li>
                </ul>
            </li>
            <li>
                <h3><strong>2</strong> Weeks before the move</h3>
                <ul>
                    <li>
                        Contact Astute Connect on 1300 854 478 or complete our online application form at <a href=""http://smartmove.myconnect.com.au/"">http://astute.myconnect.com.au/</a> to connect your<br />
                        <img class=""alignnone wp-image-1022 size-full"" src=""http://astute.myconnect.com.au/wp-content/uploads/2016/06/moving_checklist.png"" width=""1102"" height=""170"" srcset=""http://astute.myconnect.com.au/wp-content/uploads/2016/06/moving_checklist.png 1102w, http://astute.myconnect.com.au/wp-content/uploads/2016/06/moving_checklist-300x46.png 300w, h");
            WriteLiteral(@"ttp://astute.myconnect.com.au/wp-content/uploads/2016/06/moving_checklist-768x118.png 768w, http://astute.myconnect.com.au/wp-content/uploads/2016/06/moving_checklist-1024x158.png 1024w"" sizes=""(max-width: 1102px) 100vw, 1102px"" />
                    </li>
                    <li>Arrange a cleaner for your existing home once you&#8217;ve moved out</li>
                    <li>Arrange your final meter read/disconnections for your utilities</li>
                    <li>*If required* arrange for a baby sitter to look after the kids on moving day</li>
                </ul>
            </li>
            <li>
                <h3><strong>1</strong> Week before the move</h3>
                <ul>
                    <li>Organise a property inspection</li>
                    <li>Empty and clean your fridge and pantry and clean the interior of your oven</li>
                    <li>Plan ahead for the first night in your new home with sufficient clothing, toiletries, sheets and towels so you don&#8217;t hav");
            WriteLiteral(@"e to immediately sort through boxes.</li>
                    <li>Carry all important documents and valuables during your move &#8211; passports etc.</li>
                    <li>Towards the end of the week, precook a meal for the first night in your new home</li>
                </ul>
            </li>
            <li>
                <h3><strong>Moving Day!</strong></h3>
                <ul>
                    <li>Do a final check of the property before leaving</li>
                    <li>Turn off the gas, electricity, water and all switches</li>
                    <li>Hand your keys to the real estate agent/owner/tenant</li>
                    <li>Leave a note for the new occupants detailing your new address for mail redirection.</li>
                    <li>Prioritise rooms or furniture that you will need on the first night i.e. beds, cots, a few chairs if needed.</li>
                </ul>
            </li>
        </ul>


    </div>



</section>


");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
